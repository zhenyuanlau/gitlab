export const mockIntegrationProps = {
  id: 25,
  activeToggleProps: {
    initialActivated: true,
  },
  showActive: true,
  triggerFieldsProps: {
    initialTriggerCommit: false,
    initialTriggerMergeRequest: false,
    initialEnableComments: false,
  },
  jiraIssuesProps: {},
  triggerEvents: [],
  fields: [],
  type: '',
  inheritFromId: 25,
};
